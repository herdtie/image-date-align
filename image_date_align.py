#!/usr/bin/env python
"""
align images of the same event taken with cameras whose clocks were not aligned
  using exif information
"""

# Version History:
# 18/12/13 Christian Herdtweck: started creation with planning and function stubs
# 23/12/13 ch: first version of show_images is working, aligning images together
# 27/12/13 ch: kind of finished, only create_thumbs and present not implemented yet;
#   can deal with start and end args, offsets; can show imgs, save offsets, create links

from warnings import warn
from itertools import izip, count
import argparse
import cPickle
from datetime import time, date, datetime as dt, timedelta as td
import re
import os
from hashlib import md5  # for finding thumbnails
import copy

import numpy as np
import cv2

# names of exif fields that contain time, ordered by usefulness
EXIF_DATE_FIELDS = ('DateTimeOriginal', 'DateTime', 'DateTimeDigitized')


# a few variables, that are not command line args yet
NON_ARG_OPTIONS = argparse.Namespace()

# pattern used to find relevant image directories; should have a '*' in it for find_directory_descriptions
NON_ARG_OPTIONS.img_dir_pattern = os.path.expanduser('~/photos/Hochzeit/von_*')

# file where image dates and directory offsets are saved
NON_ARG_OPTIONS.data_file = 'image_date_align.pickle'

# thumbnails, should work on modern linux systems
NON_ARG_OPTIONS.thumb_file_pattern = os.path.expanduser('~/.thumbnails/normal/{0}.png')
NON_ARG_OPTIONS.thumb_size = 128

# output for createLinks
NON_ARG_OPTIONS.link_file_pattern = os.path.expanduser('~/photos/Hochzeit/Angeordnet_nach_datum/{0}_{1}_{2}')
  # 0=date, 1=dir_desc, 2=file name



def main():
  r""" called when somebody calls this file as script on the command line

  createThumbs will create thumbnails of images in dirs corresponding 
    to img_dir_pattern 
    Possible options: thumbSize

  printDates will print a summary of exif/file dates per image dir
    Possible options: start end offset_string

  showImgs will show a list of thumbnails, ordered by their dates, to optimize offsets
    Possible options: start end offset_string

  writeOffsets: save offsets to data_file, so do not have to give them as options andy more
    Possible options: start end offset_string

  present will show one full image after the other in temporal order
    Possible options: start end

  createLinks will create a new directory with links to all image files, where link names are
    image dates, so traversal of this folder will show images by date

  updateFiles will look again for image files, in case new ones have been added or some removed
  """
  # define what options are used by which command
  # make sure this is in sync with options used when calling functions!
  used_options = {}
  used_options['createThumbs'] = ('img_dir_pattern', 'data_file', 'thumb_size', 'thumb_file_pattern')
  used_options['printDates']   = ('img_dir_pattern', 'data_file', 'offsets')
  used_options['showImgs']     = ('img_dir_pattern', 'data_file', 'offsets', 'start', 'end', \
                                  'thumb_size', 'thumb_file_pattern', 'link_file_pattern')
  used_options['writeOffsets'] = ('img_dir_pattern', 'data_file', 'offsets')
  used_options['present']      = ('img_dir_pattern', 'data_file', 'offsets')
  used_options['createLinks']  = ('img_dir_pattern', 'data_file', 'offsets', 'start', 'end', \
                                  'link_file_pattern')
  used_options['updateFiles']  = ('img_dir_pattern', 'data_file')

  # parse arguments
  cmd, options, default_options = parse_args()

  # check whether unused options are given
  option_names = vars(options).keys()
  option_dict = vars(options)
  default_dict = vars(default_options)
  assert cmd in used_options.keys(), 'cmd accepted in parse_args but not known in main!'
  allowed_options = used_options[cmd]
  for option,val in option_dict.iteritems():
    if option not in allowed_options:
      if val == default_dict[option]:   # if option has default value, that is like not given
        continue
      warn('ignoring given option "{0}"!'.format(option))
  #end: for all given options

  # now call appropriate function with args
  # make sure that args here are same as specified in used_options!
  if cmd == 'createThumbs':
    create_thumbs(options.img_dir_pattern, options.data_file, options.thumb_size, options.thumb_file_pattern)
  elif cmd == 'printDates':
    print_dates(options.img_dir_pattern, options.data_file, options.offsets)
  elif cmd == 'showImgs':
    show_images(options.img_dir_pattern, options.data_file, options.offsets, options.start, options.end, \
                options.thumb_size, options.thumb_file_pattern)
  elif cmd == 'writeOffsets':
    write_offsets(options.img_dir_pattern, options.data_file, options.offsets)
  elif cmd == 'present':
    present(options.img_dir_pattern, options.data_file, options.offsets)
  elif cmd == 'createLinks':
    create_file_links(options.img_dir_pattern, options.data_file, options.offsets, options.start, options.end, \
        options.link_file_pattern)
  elif cmd == 'updateFiles':
    update_files(options.img_dir_pattern, options.data_file)
  else:
    raise ValueError('unknown command: "{0}"!'.format(cmd))   # should not happen
#end: function main


def parse_args():
  r""" parse command line arguments 

  :returns: command to run and option struct

  intended usage:
    file_name [-option 1] [-option 2] [...] command
    where command is one of createThumbs printDates showImgs writeOffsets, present, createLinks, updateFiles
    and not all options make sense for all commands

    Internal variables of file_name are
    * img_dir_pattern
    * precomp_file
  """
  
  # create parser
  parser = argparse.ArgumentParser(\
      description='Align capturing times of images of the same event taken by different cameras',\
      epilog='created by Christian Herdtweck, christian.herdtweck@gmail.com')
  parser.add_argument('command', help='the action to perform', \
      choices=['createThumbs','printDates','showImgs','writeOffsets', 'present', 'createLinks', 'updateFiles'])
  parser.add_argument('-s', '--start', help='only deal with images taken after this date')
  parser.add_argument('-e', '--end', help='only deal with images taken before this date')
  parser.add_argument('-o', '--offsets', help='offsets for each folder; USE THIS ONLY ONCE (see README file)', nargs='*')
  #parser.add_argument('offsets', help='offsets for each folder', nargs=argparse.REMAINDER)

  # define default options
  default_options = copy.deepcopy(NON_ARG_OPTIONS)
  default_options.start = None
  default_options.end = None
  default_options.offsets = None

  # parse args, convert result into dict
  options = parser.parse_args(namespace=default_options)

  # get command out of options
  command = options.command
  del options.command

  # convert start/end dates
  # are required to be datetime [H]H:MM[:SS], end can be delta: +1y2d3h4m5.67s
  if options.start is not None:
    options.start = parse_datetime(options.start)
  if options.end is not None:
    if options.end[0] == '+':
      options.end = ('+', parse_timedelta(options.end))
    else:
      options.end = parse_datetime(options.end)

  # parse offsets
  # offsets are of form DESC-1m or DESC+2h3s or DESC-3.54s or DESC+323d5h; 
  #   m stands for minutes, d for days, h for hours, s for seconds
  #   all must be int except for seconds which can have fractions
  new_offsets = {}
  if options.offsets is not None:
    for offset in options.offsets:
      matches = re.match('(.+)([+-])([\ddhmsDHMS]+)', offset)
      # --> this even allows - and + signs in dir_descs :-)
      if matches is None:
        raise ValueError('offset string "{0}" does not conform to DESC+TIME nor DESC-TIME!'.format(offset))
      dir_desc, sign, time_str = matches.groups()
      new_offsets[dir_desc] = (sign, parse_timedelta(time_str))
  #end: for all offsets
  options.offsets = new_offsets

  # done
  #print options
  return command, options, default_options
#end: parse_args


def parse_timedelta(str):
  matches = re.match('(?:(\d+)d)?(?:(\d+)h)?(?:(\d+)m)?(?:([\d.]+)s)?', str.lower())
  if matches is None:
    raise ValueError('string "{0}" does not conform to timedelta format [Nd][Mh][Lm][Ks] where NML are int, K float!'\
        .format(str))
  days,hours, mins, secs = matches.groups()
  if days is None:
    days = 0
  else:
    days = int(days)
  if hours is None:
    hours = 0
  else:
    hours = int(hours)
  if mins is None:
    mins = 0
  else:
    mins = int(mins)
  if secs is None:
    secs = 0
  else:
    secs = float(secs)
  return td(days=days, seconds=secs+60*(mins+60*hours))
#end: parse_timedelta


def parse_datetime(str):
  r""" returns either a datetime or a time, depending on whether date info is given """
  matches = re.match('(?:(\d{2})/(\d{2})/(\d{2,4}),)?(\d{1,2}):(\d{2})(?::(\d{2}))?', str.lower())
  if matches is None:
    raise ValueError('cannot interpret start date "{0}", needs to conform to [DD/MM/YY,][H]H:MM[:SS.SSS]!'.format(str))
  else:
    day,month,year,hours, mins, secs = matches.groups()
    if secs is None:   # secs can be omitted, but there is known default
      secs = 0
    if day is None:
      return time(int(hours), int(mins), int(secs))
    else:
      return datetime(int(year), int(month), int(day), int(hours), int(mins), float(secs))
#end: parse_datetime


def strfTimeDelta(td, maxParts=np.inf):
  r""" string representation of a TimeDelta object i.e., a duration

  arguments: a timedelta object and an optional maxParts (defaults to inf)
    max number of string parts that are to be displayed
  """

  # divide micro seconds in parts of seconds, minutes, hours, days, months, years and so on
  ratios = np.array((1000, 60, 60, 24, 30, 12, 10, 10, 10))  # ratios from one unit to other (e.g. 1 hour = 60 minutes)
  units = ('ms', 's', 'm', 'h', 'd', 'mon', 'y', 'decade[s]', 'centurie[s]', 'milleni[um/a]')  # name of units

  textParts = []

  # initialize to ms (= microseconds / 1000)
  partIdx = 0
  if td.microseconds > 0:
    textParts.append('{0:.1f}{1},'.format(float(td.microseconds)/float(ratios[partIdx]), units[partIdx]))

  # divide seconds into minutes and hours
  toDivide = td.seconds
  while toDivide > 0:
    partIdx += 1
    toDivide, remainder = divmod(toDivide, ratios[partIdx])
    if remainder > 0:
      textParts.append('{0:n}{1},'.format(remainder, units[partIdx]))

  # divide days into months and years and so on
  toDivide += td.days
  partIdx = 3
  while toDivide > 0:
    partIdx += 1
    toDivide, remainder = divmod(toDivide, ratios[partIdx])
    if remainder > 0:
      textParts.append('{0:n}{1},'.format(remainder, units[partIdx]))

  # now use last few parts and assemble string from that
  if len(textParts) == 0:
    returnString = '0 '
  elif len(textParts) <= maxParts:
    returnString = ''.join(textParts[::-1])
  else:
    returnString = ''.join(textParts[-1:-1-maxParts:-1])

  return returnString[0:-1]
#end: function strfTimeDelta


def analyze_folder(folder, verbose=False):
  r""" search folder for image files and their dates either from exif or file date 

  called by find_imgs_and_dates , copied from imgDateAlign.py
  """

  from PIL import Image
  from PIL.ExifTags import TAGS

  print 'analyzing folder', folder
  imgFiles = []
  dates = []
  isExif = []
  folderContents = sorted(os.listdir(folder))
  folders = []
  for fileName in folderContents:
    fullFile = os.path.join(folder,fileName)
    if os.path.isdir(fullFile):
      folders.append(fullFile)
      continue

    match = re.match('.+\.(jpg|jpeg|tif|tiff|png|bmp|ppm|gif)$',fileName,re.IGNORECASE)
    if match is None:
      # not even an image
      if verbose:
        print fullFile, 'is no img'
      continue

    # now know that this is an img
    imgFiles.append(fullFile)
    fileDate = dt.fromtimestamp(os.stat(fullFile).st_ctime)

    match = re.match('.+\.(jpg|jpeg)$',fileName,re.IGNORECASE)
    if match is None:
      if verbose:
        print fullFile, 'is an img but not jpg; date is', fileDate
      dates.append(fileDate)
      isExif.append(False)
      continue

    # now know that it is a jpg
    try:
      allExifData = Image.open(fullFile)._getexif()  # --> ist ein dictionary
    except IOError:
      dates.append(fileDate)
      isExif.append(False)
      if verbose:
        print fullFile, 'is jpg but cannot read exif!'
      continue

    if allExifData is None:
      dates.append(fileDate)
      isExif.append(False)
      if verbose:
        print fullFile, 'is jpg but exif is empty'
      continue

    verfuegbare_felder_mit_codes = dict( \
        (TAGS.get(feld_code),feld_code) for feld_code in allExifData.iterkeys() )
    #print [beschr for beschr,code in verfuegbare_felder_mit_codes.iteritems()]

    # teste ob eines der gewuenschten dabei ist
    haveExif = False
    for gewuenschtes_feld in EXIF_DATE_FIELDS:
      if gewuenschtes_feld in verfuegbare_felder_mit_codes:
        datums_text = allExifData[verfuegbare_felder_mit_codes[gewuenschtes_feld]]
        
        # konvertiere Datum in numerisches format
        try:
          if verbose:
            print fullFile, 'is jpg taken', datums_text
          dates.append(dt.strptime(datums_text, '%Y:%m:%d %H:%M:%S'))
          isExif.append(True)
          haveExif = True
          break
        except ValueError:
          # anscheinend hat das Datum ein unerwartetes format. gib warnung aus
          #   und versuche weiter
          warn('Datumstext "{0}" hat das falsche Format!'.format(datums_text))
    # --> neues_datum ist None oder ein datetime-Objekt

    if not haveExif:
      if verbose:
        print fullFile, 'is jpg with exif but no date, use file date', fileDate
      dates.append(fileDate)
      isExif.append(False)
      
  #end: for each file in dir

  if len(dates) == 0:
    print '  no imgs'
  else:
    dates = np.asarray(dates)
    print '  found', len(dates), 'images,', sum(isExif), 'of which have exif dates. Exif/file dates range from', dates.min(), 'to', dates.max()

  # recurse into subfolders
  for subFolder in folders:
    if subFolder.endswith('raw'):
      continue
    analyze_folder(subFolder)

  return imgFiles, np.asarray(dates)
#end: analyze_folder


def find_imgs_and_dates(img_dir_pattern):
  r""" find image dirs and exif dates 
  
  called if load_data has no saved data for img_dir_pattern or
    if that needs to be recomputed

  :returns: (img_dirs, files_per_dir)
  """

  from glob import glob

  img_dirs = glob(img_dir_pattern)
  files_per_dir = []
  dates_per_dir = []

  for img_dir in img_dirs:
    img_files, file_dates = analyze_folder(img_dir)
    files_per_dir.append(img_files)
    dates_per_dir.append(file_dates)

  return img_dirs, files_per_dir, dates_per_dir
#end: find_imgs_and_dates


def update_files(img_dir_pattern, data_file):
  r""" update info on dirs and files, preserving old data as much as possible """

  # remember old data, for current pattern and for all others as well
  (old_img_dirs, old_files_per_dir, old_dates_per_dir, old_offsets), all_data = load_data(img_dir_pattern, data_file, want_all=True)

  # find new dirs
  new_img_dirs, new_files_per_dir, new_dates_per_dir = find_imgs_and_dates(img_dir_pattern)

  # remember which old dir has been used
  old_used = np.array([False for img_dir in old_img_dirs])

  # loop over new image dirs, trying to get offsets from corresponding old dirs
  new_offsets = []
  for img_dir, files_per_dir in izip(new_img_dirs, new_files_per_dir):
    idx_in_old = [idx for idx,old_dir in enumerate(old_img_dirs) if old_dir == img_dir]
    if len(idx_in_old) == 0:
      # dir does not exist in old_img_dirs
      print 'add new dir', img_dir
      new_offsets.append(td(0))
    elif len(idx_in_old) == 1:
      # found corresponding dir in img_dirs
      idx_in_old = idx_in_old[0]
      new_offsets.append(old_offsets[idx_in_old])
      print 'keep offset', old_offsets[idx_in_old], 'for dir', img_dir,
      print '(old: {0} imgs; new: {1} imgs)'.format(len(old_files_per_dir[idx_in_old]), len(files_per_dir))
      old_used[idx_in_old] = True
    else:
      raise Exception('two dirs that are equal to img_dir??? should not be possible!')
  #end: loop over dirs

  # print info on which dir was used
  if not old_used.all():
    print 'could not find following dir(s) any more:'

    for was_used, img_dir, offset, files_per_dir in izip(old_used, old_img_dirs, old_offsets, old_files_per_dir):
      if was_used:
        continue
      print img_dir, 'with', len(files_per_dir), 'imgs and offset', offset

  all_data[img_dir_pattern] = new_img_dirs, new_files_per_dir, new_dates_per_dir, new_offsets 
  write_data(all_data, data_file)

#end: update_files



def load_data(img_dir_pattern, data_file, want_all=False):
  r""" load precompuated data from data files or calls find_imgs_and_dates
  
  :returns: (img_dirs, files_per_dir, dates_per_dir, dir_offsets)
  """

  if not os.path.exists(data_file):
    print 'no data file found, creating new one'
    imgs_per_pattern = {}
  else:
    # load data file
    with open(data_file, 'r') as f:
      imgs_per_pattern = cPickle.load(f)

  # if img_dir_pattern not in data file, then need to parse dirs
  if img_dir_pattern not in imgs_per_pattern:
    print 'no data for image dir pattern', img_dir_pattern, 'in', data_file, 'so looking for images now'

    # if not found call find_imgs_and_dates and set dir_offsets to 0
    img_dirs, files_per_dir, dates_per_dir = find_imgs_and_dates(img_dir_pattern)
    dir_offsets = [td(0) for img_dir in img_dirs]

    # save (only reach this point if img_dir_pattern not in data_file)
    imgs_per_pattern[img_dir_pattern] = img_dirs, files_per_dir, dates_per_dir, dir_offsets 
    write_data(imgs_per_pattern, data_file)

  if want_all:
    return imgs_per_pattern[img_dir_pattern], imgs_per_pattern
  else:
    return imgs_per_pattern[img_dir_pattern]
    
#end: load_data


def write_data(data, data_file):
  r""" actually write data to data file, called by load_data and write_offsets """

  # ask for confirmation
  print 'do you really want to write to', data_file, '?'
  answer = user_input_choice('y', 'n')
  if answer == 'y':
    with open(data_file, 'w') as f:
      cPickle.dump(data, f)
    print 'written data to', data_file
    return True
  else:
    print 'not writing'
    return False
#end: write_data



def write_offsets(img_dir_pattern, data_file, offset_args):
  r""" save offsets to data file """

  (img_dirs, files_per_dir, dates_per_dir, offsets), all_data = load_data(img_dir_pattern, data_file, want_all=True)
  offsets = adjust_offsets(img_dirs, offsets, offset_args)

  all_data[img_dir_pattern] = img_dirs, files_per_dir, dates_per_dir, offsets 
  write_data(all_data, data_file)
#end: write_offsets


def adjust_offsets(img_dirs, offsets, offset_args):
  r""" add offsets from options to those from file; check whether offsets were given for non-existent dirs 
  
  :param offsets: dict with key=dir and value=timedelta
  :param offset_args: dict like offsets, but key can be much shortened
  """

  unused_args = []
  for dir_desc,arg_offset in offset_args.iteritems():

    if dir_desc in img_dirs:
      # simple case: dir_desc is whole dir name
      offsets[dir_desc] += arg_offset
    else:
      # check whether dir_desc describes a unique img_dir
      matches = []
      for i_dir,img_dir in enumerate(img_dirs):
        if dir_desc.lower() in img_dir.lower():
          matches.append(i_dir)

      if len(matches) == 0:
        # don't know what dir_desc refers to
        warn('found no img dir matching "{0}" -- ignore it!'.format(dir_desc))
        continue
      elif len(matches) == 1:
        # perfekt!
        #print '"{0}" matches img dir {1} uniquely'.format(dir_desc, matches[0])
        if arg_offset[0] == '+':
          offsets[matches[0]] += arg_offset[1]
        else:
          offsets[matches[0]] -= arg_offset[1]
      else:
        # no unique match
        warn('"{0}" has several possible img dirs as match: {1} -- ignore it!'.format(dir_desc, matches))
        continue
    #end: dir_desc is a dir
  #end: for all offset_args

  return offsets
#end: adjust_offsets


def create_thumbs(img_dir_pattern, data_file, thumb_size, thumb_file_pattern):
  r""" create thumbnails in thumb_dir of images in all image dirs (dirs corresponding to img_dir_pattern) 
  
  on linux, thumbnails have a size of NxM and are saved in ~/.thumbnails/normal/hash_value.png where
    hash_value is a md5-hash of the file name; either N or M is 128, the other is smaller such that
    the image aspect ratio stays approx. the same
  """

  import hashlib

  # check for unused options: from, to, offsets

  for img_dir, img_files, img_dates, _ in izip(load_data(img_dir_pattern, data_file)):
    file_name = os.path.join(img_dir, img_files)
    thumb_file_name = thumb_file_for_file(file_name)

    raise NotImplementedError()
#end: create_thumbs


def print_dates(img_dir_pattern, data_file, offset_args):
  r""" summarize file dates per folder and print to console """

  img_dirs, files_per_dir, dates_per_dir, offsets = load_data(img_dir_pattern, data_file)
  offsets = adjust_offsets(img_dirs, offsets, offset_args)

  max_dir_len = max( len(img_dir) for img_dir in img_dirs )

  # loop over image dirs
  for img_dir, img_files, img_dates, offset in izip(img_dirs, files_per_dir, dates_per_dir, offsets):
    if len(img_dates) == 0:
      print '{0}:{1}  no files'.format(img_dir, ' '*(max_dir_len - len(img_dir)))
    else:
      min_date = img_dates.min() + offset
      max_date = img_dates.max() + offset
      n_no_date = '?'
      print '{0}:{6}{1: 4d} files, dates in {3} - {4} after offset {5}'.format(\
          img_dir, len(img_files), n_no_date, min_date, max_date, strfTimeDelta(offset), ' '*(max_dir_len - len(img_dir)))

  print 'overall:', sum( len(img_dates) for img_dates in dates_per_dir ), 'imgs'
#end: print_dates
    

def show_images(img_dir_pattern, data_file, offset_args, start_date, end_date, thumb_size, thumb_file_pattern, n_thumbs=None):
  r""" show a few thumbnails ordered by date """

  # params
  win_name = 'image_date_align'
  gap_x = 5
  gap_y = 5
  gap_color = (0,0,0,0)
  text_color = (255,255,255,0)
  text_font = cv2.FONT_HERSHEY_SIMPLEX
  text_scale = .5
  text_thickness = 2
  text_offset_dir = 12
  text_offset_time = 30
  continue_keys = ( 46, 1048622, 536870958, # dot \
                    32, 1048608, 536870944, # space \
                   106) # j
  stop_keys = (113, 1048689, 536871025, 537919601)  # q

  # get data and adjust offsets
  img_dirs, files_per_dir, dates_per_dir, offsets = load_data(img_dir_pattern, data_file)
  offsets = adjust_offsets(img_dirs, offsets, offset_args)

  # fill in day of start/end date
  if isinstance(start_date, time):
    # need to find reasonable date for time; create hist of start dates per dir and use most common
    date_only = most_frequent_date( np.min(dates_curr_dir).date() for dates_curr_dir in dates_per_dir if len(dates_curr_dir) > 0)
    start_date = dt.combine(date_only, start_date)
  if isinstance(end_date, time):
    # need to find reasonable date for time; create hist of end dates per dir and use most common
    date_only = most_frequent_date( np.max(dates_curr_dir).date() for dates_curr_dir in dates_per_dir if len(dates_curr_dir) > 0)
    end_date = dt.combine(date_only, end_date)

  # find text strings to well describe folders
  dir_descs = find_directory_descriptions(img_dirs, img_dir_pattern)

  # print info
  if (start_date is not None) or (end_date is not None):
    print 'showing images taken between', start_date, 'and', end_date
  for desc, offset in izip(dir_descs, offsets):
    print desc, strfTimeDelta(offset)

  # create an object for sorting files and selecting appropriate dates
  date_sorter = iter(ParallelDateSorter(dates_per_dir, offsets, start_date, end_date))

  # identify number of thumbnails that will fit on the screen with gaps
  size_x = thumb_size + gap_x
  size_y = thumb_size + gap_y
  if n_thumbs is None:
    screen_width, screen_height = get_screen_size()
    n_x = screen_width / size_x
    n_y = screen_height / size_y
  else:
    n_x, n_y = n_thumbs

  # create big buffer to fit grid of thumbs; fill with gap color
  big_img = np.zeros( (n_y * size_y - gap_y, n_x * size_x - gap_x, 4), dtype=np.uint8)
  for i_dim in range(4):
    big_img[:,:,i_dim] = gap_color[i_dim]

  # one big try-except to ensure win is closed and deal with StopIteration
  try:
    # fill buffer (reading order)
    for i_y in range(n_y):  # downward
      for i_x in range(n_x):  # rightward
        # identify next file
        next_folder, next_file, next_date = next(date_sorter)

        # identify corresponding img file
        img_file_name = files_per_dir[next_folder][next_file]

        # load corresponding thumb
        try:
          thumb_data = cv2.imread(thumb_file_for_file(img_file_name, thumb_file_pattern))
          thumb_height, thumb_width, thumb_channels = thumb_data.shape
        
          # fine-tune its position
          thumb_height, thumb_width, thumb_channels = thumb_data.shape
          offset_x = (thumb_size - thumb_width) / 2
          offset_y = (thumb_size - thumb_height) / 2

          # copy to buffer
          top = i_y * size_y + offset_y
          lft = i_x * size_x + offset_x
          big_img[top : top+thumb_height, lft : lft+thumb_width, 0 : thumb_channels] = thumb_data

        except:
          warn('no thumbnail for image {0}!'.format(img_file_name))

        # write folder and time
        cv2.putText(big_img, dir_descs[next_folder], (i_x*size_x, i_y*size_y+text_offset_dir), text_font, \
            text_scale, (0,0,0,0), text_thickness+2, cv2.CV_AA)
        cv2.putText(big_img, dir_descs[next_folder], (i_x*size_x, i_y*size_y+text_offset_dir), text_font, \
            text_scale, text_color, text_thickness, cv2.CV_AA)
        cv2.putText(big_img, next_date.strftime('%H:%M:%S'), (i_x*size_x, (i_y)*size_y+text_offset_time), text_font, \
            text_scale, (0,0,0,0), text_thickness+2, cv2.CV_AA)
        cv2.putText(big_img, next_date.strftime('%H:%M:%S'), (i_x*size_x, (i_y)*size_y+text_offset_time), text_font, \
            text_scale, text_color, text_thickness, cv2.CV_AA)
    #end: for all positions in thumb grid

    # show buffer
    cv2.imshow(win_name, big_img)

    # infinite loop with user input
    # first version: simple, only scroll downward
    # next version: save old lines that are not on the screen any more to allow upward scrolling
    # even better: pre-load next lines
    i_y = n_y - 1
    while True:
      # get user feedback
      key = cv2.waitKey(100)
      
      # interpret command
      if key == -1:
        continue
      elif key in stop_keys:
        print 'user wants to quit'
        break
      elif key not in continue_keys:
        print 'no action for key', key
        continue
      # reach this point only if key is continue_key

      # move buffer up by one image
      big_img[: i_y*size_y-gap_y, :, :] = big_img[size_y:, :, :]
   
      # fill with bg
      for i_dim in range(4):
        big_img[i_y * size_y:,:,i_dim] = gap_color[i_dim]

      # add new line
      for i_x in range(n_x):  # rightward
        # identify next file
        next_folder, next_file, next_date = next(date_sorter)

        # identify corresponding img file
        img_file_name = files_per_dir[next_folder][next_file]

        # load corresponding thumb
        try:
          thumb_data = cv2.imread(thumb_file_for_file(img_file_name, thumb_file_pattern))
          thumb_height, thumb_width, thumb_channels = thumb_data.shape

          # fine-tune its position
          offset_x = (thumb_size - thumb_width) / 2
          offset_y = (thumb_size - thumb_height) / 2

          # copy to buffer
          top = i_y * size_y + offset_y
          lft = i_x * size_x + offset_x
          big_img[top : top+thumb_height, lft : lft+thumb_width, 0 : thumb_channels] = thumb_data

        except:
          warn('no thumbnail for image {0}!'.format(img_file_name))

        # write folder and time
        cv2.putText(big_img, dir_descs[next_folder], (i_x*size_x, i_y*size_y+text_offset_dir), text_font, \
            text_scale, (0,0,0,0), text_thickness+2, cv2.CV_AA)
        cv2.putText(big_img, dir_descs[next_folder], (i_x*size_x, i_y*size_y+text_offset_dir), text_font, \
            text_scale, text_color, text_thickness, cv2.CV_AA)
        cv2.putText(big_img, next_date.strftime('%H:%M:%S'), (i_x*size_x, (i_y)*size_y+text_offset_time), text_font, \
            text_scale, (0,0,0,0), text_thickness+2, cv2.CV_AA)
        cv2.putText(big_img, next_date.strftime('%H:%M:%S'), (i_x*size_x, (i_y)*size_y+text_offset_time), text_font, \
            text_scale, text_color, text_thickness, cv2.CV_AA)
      #end: for all files in new line

      # show
      cv2.imshow(win_name, big_img)
    #end: while True

  except StopIteration:
    cv2.imshow(win_name, big_img)
    print 'reached end, one more key to end'
    cv2.waitKey()
  except:
    raise
  finally:
    # destroy window
    cv2.destroyWindow(win_name)

#end: show_images


def create_file_links(img_dir_pattern, data_file, offset_args, start_date, end_date, link_file_pattern):
  r""" create directory with links to files, where link names are date and time """
  img_dirs, files_per_dir, dates_per_dir, offsets = load_data(img_dir_pattern, data_file)
  offsets = adjust_offsets(img_dirs, offsets, offset_args)

  dir_descs = find_directory_descriptions(img_dirs, img_dir_pattern)
  
  # fill in day of start/end date
  if isinstance(start_date, time):
    # need to find reasonable date for time; create hist of start dates per dir and use most common
    date_only = most_frequent_date( np.min(dates_curr_dir).date() for dates_curr_dir in dates_per_dir if len(dates_curr_dir) > 0)
    start_date = dt.combine(date_only, start_date)
  if isinstance(end_date, time):
    # need to find reasonable date for time; create hist of end dates per dir and use most common
    date_only = most_frequent_date( np.max(dates_curr_dir).date() for dates_curr_dir in dates_per_dir if len(dates_curr_dir) > 0)
    end_date = dt.combine(date_only, end_date)

  # find text strings to well describe folders
  dir_descs = find_directory_descriptions(img_dirs, img_dir_pattern)

  # print info
  if (start_date is not None) or (end_date is not None):
    print 'creating links for images taken between', start_date, 'and', end_date
  for desc, offset in izip(dir_descs, offsets):
    print desc, strfTimeDelta(offset)

  date_sorter = iter(ParallelDateSorter(dates_per_dir, offsets, start_date, end_date))

  # ensure dir exists
  path_part, _ = os.path.split(link_file_pattern.format(0,0,0))
  if not os.path.exists(path_part):
    raise ValueError('directory for links of type {0} does not exist!'.format(link_file_pattern))

  for next_folder, next_file, next_date in date_sorter:
    file_name = files_per_dir[next_folder][next_file]
    _, file_name_part = os.path.split(file_name)
    link_name = link_file_pattern.format(next_date.strftime('%y%m%d_%H%M%S'), dir_descs[next_folder], file_name_part)
    #print link_name
    os.symlink(file_name, link_name)

  print 'done'
#end: function create_file_links

def present(img_dir_pattern, data_file, offset_args, start_date, end_date):
  r""" show full images in temporal order """
  img_dirs, files_per_dir, dates_per_dir, offsets = load_data(img_dir_pattern, data_file)
  offsets = adjust_offsets(img_dirs, offsets, offset_args)

  date_sorter = iter(ParallelDateSorter(dates_per_dir, offsets, start_date, end_date))
  raise NotImplementedError()
#end: function present



class ParallelDateSorter(object):
  r""" object to simplify finding files in limited number of dirs with successive dates 
  
  does not copy file names or dates, only returns indices of folder and file within folder
  """

  _n_dirs = None
  _order_per_dir = None
  _dates_per_dir = None
  start_date = None
  end_date = None

  def __init__(self, dates_per_dir, offsets, start_date=None, end_date=None):
    r""" sorts files within each dir by date; initializes _next_per_dir with 0s 

    start_date and/or end_date can be None for no limit
    """

    if len(dates_per_dir) != len(offsets):
      raise ValueError('have different number of sets in dates and offsets!')
    self._n_dirs = len(dates_per_dir)

    # quick check for dates
    if (start_date is not None) and (end_date is not None):
      if start_date > end_date:
        warn('start date {0} is after end date {1}!'.format(start_date, end_date))
    self.start_date = start_date
    self.end_date = end_date

    # create sort order for each date set, add offset and remove stuff before start and after end
    self._order_per_dir = []
    self._dates_per_dir = []
    for dates, offset in izip(dates_per_dir, offsets):
      curr_order = np.argsort(dates)
      curr_dates = dates[curr_order] + offset

      if start_date is not None:
        keep = curr_dates >= start_date
        curr_order = curr_order[keep]
        curr_dates = curr_dates[keep]
      if end_date is not None:
        keep = curr_dates <= end_date
        curr_order = curr_order[keep]
        curr_dates = curr_dates[keep]

      self._order_per_dir.append(curr_order)
      self._dates_per_dir.append(curr_dates)
    #end: for all sets

  #end: ParallelDateSorter constructor


  def __str__(self):
    r""" return legible string describing an object of this class """
    text = '[ParallelDateSorter for {0} dirs'.format(self._n_dirs)
    if self.start_date is None:
      if self.end_date is None:
        pass
      else:
        text += 'before {0}'.format(self.end_date)
    else:   # have start date
      if self.end_date is None:
        text += 'after {0}'.format(self.start_date)
      else:
        text += 'between {0} and {1}'.format(self.start_date, self.end_date)
    return text + ']'
  #end: ParallelDateSorter.__str__


  def __iter__(self):
    r""" checks in each dir, which next file has the lowest date and returns reference to that file
   
    :returns: folder_idx, file_idx, file_date
    """

    idx_per_dir = np.zeros(self._n_dirs, dtype=int)

    while True:
      best_date = None
      best_folder = None
      best_idx = None
      for i_folder, curr_dates, curr_idx in izip(count(), self._dates_per_dir, idx_per_dir):
        if curr_idx == len(curr_dates):
          continue  # shown all imgs in this folder
        elif best_date is None:
          best_date = curr_dates[curr_idx]
          best_folder = i_folder
          best_idx = curr_idx
        elif best_date > curr_dates[curr_idx]:
          # next img in this folder is earlier
          best_date = curr_dates[curr_idx]
          best_folder = i_folder
          best_idx = curr_idx
        else:
          # next img in this folder is later
          continue
      if best_date is None:
        raise StopIteration()
      else:
        yield best_folder, self._order_per_dir[best_folder][best_idx], best_date
        idx_per_dir[best_folder] += 1
    #end: inf loop
  #end: ParallelDateSorter.__iter__

#end: class ParallelDateSorter


def get_screen_size():
  """ query screen size in pixels using system calls

  may raise ValueError, OSError, IOError
  returns width, height of probably first screen
  uses code from 
  http://stackoverflow.com/questions/3129322/how-do-i-get-monitor-resolution-in-python
  """

  if os.name is 'nt':
    from ctypes import windll
    user32 = windll.user32
    return user32.GetSystemMetrics(0), user32.GetSystemMetrics(1)
  elif os.name is 'posix':  # linux
    # preparations
    import subprocess
    command = 'xwininfo -root'
    screenSizeRegexp = re.compile('\s*(?:(?:Width:\s*(?P<width>\d*))|(?:Height:\s*(?P<num>\d*)))\s*')
    try:
      # run shell command xwininfo
      proc = subprocess.Popen(command, shell=True, stdout = subprocess.PIPE, close_fds=True)
      proc.wait()   # above starts process already, here make sure it has returned
      code = proc.returncode
      if code < 0:
        raise ValueError('{0} was terminated by signal {1}!'.format(command, -code))
        #print '{0} was terminated by signal {1}!'.format(command, -code)
        #print 'return fixed values 1200x1600'
        #return 1200,1600
      elif code > 0:
        raise ValueError('{0} returned {1}!'.format(command, code))
        #print '{0} returned {1}!'.format(command, code)
        #print 'return fixed values 1200x1600'
        #return 1200,1600
      
      # interpret result string
      matches = [screenSizeRegexp.match(line).groups() for line in proc.stdout if screenSizeRegexp.match(line)]
    except OSError, e:
      print 'Execution of {0} failed: {1}'.format(command, e)
      raise
      #print 'return fixed values 1200x1600'
      #return 1200,1600
    except IOError as (errno, strerror):
      print "I/O error({0}) interpreting output of {1}: {2}".format(errno, command, strerror)
      raise
      #print 'return fixed values 1200x1600'
      #return 1200,1600

    try:
      widths = [int(matchPair[0]) for matchPair in matches if matchPair[0]]
      heights = [int(matchPair[1]) for matchPair in matches if matchPair[1]]
    except ValueError, ve:
      print "Value error interpreting output {0} of {1} as ints: {2}".format(matches, command, ve)
      raise

    if len(widths) > 1 or len(heights) > 1:
      raise ValueError('too many widths {0} or heights {1} found in output of {2}!'.format(widths, heights, command))

    return widths[0], heights[0]
  else:
    # on mac os x could try /usr/sbin/system_profiler SPDisplaysDataType | grep Resolution
    import AppKit
    return [(screen.frame().size.width, screen.frame().size.height)
        for screen in AppKit.NSScreen.screens()]
    raise OSError('get_screen_size currently only runs on posix-type systems (like for example linux)!')

#end: function get_screen_size


def user_input_choice(*choices):
  r""" give user a few choices for input until he chose one

  use as follows:
    print 'please select o for option1, opt for option2, 3 for option3, or b for option4',
    keySelected = user_input_choice(o, opt, 3, b)
    text on cmd line: give one [o,opt,3,b]: opt <return>
    --> will return 'opt'

  todo: might add possibility to define choices as dict or list and not with keywords and use first letter as shorthand
  """
  # make sure there is a choices[-1]
  if len(choices) < 1:
    raise ValueError('need to give user at least one choice!')

  # modify prompt text to include options in square brackets
  #if promptText is None:
  promptText = '[' + ''.join([c+',' for c in choices[:-1]]) + choices[-1] + ']: '
  #else:
  #  promptText = promptText + '[' + ''.join([c+',' for c in choices[:-1]]) + choices[-1] + ']'

  # now until user has entered one of choices:
  while True:
    # show the prompt with choices
    inputText = raw_input(promptText)

    # see whether was any of choices
    for c in choices:
      if inputText == c:
        return c

    # if reach here it was none of choices, so complain and start again
    print '\'{0}\' is none of choices given - try again.'.format(inputText),
  #end: while True
#end: function user_input_choice


def thumb_file_for_file(file_name, thumb_file_pattern): 
  thumb_hash = md5('file://' + file_name).hexdigest()
  return thumb_file_pattern.format(thumb_hash)


def find_directory_descriptions(img_dirs, img_dir_pattern):
  r""" assume that img_dir_pattern has a * in it and that that is the interesting part """
  
  re_str = img_dir_pattern.replace('*', '(.*)')
  descs = []
  for img_dir in img_dirs:
    matches = re.match(re_str, img_dir)
    if matches is None:
      raise ValueError('img dir {0} seems to not conform to pattern {1}!'.format(img_dir, img_dir_pattern))
    else:
      descs.append(''.join(matches.groups()))

  return descs
#end: find_directory_descriptions


def most_frequent_date(date_iter):
  r""" if user specifies only time on the command line for start and/or end, use this to guess the day

  creates a histogram of image dates and returns the most frequent
  """
  # create hist
  hist = {}
  for date in date_iter:
    if date in hist.keys():
      hist[date] += 1
    else:
      hist[date] = 1

  # find max
  max_date = None
  max_count = 0
  for curr_date, curr_count in hist.iteritems():
    if curr_count > max_count:
      max_count = curr_count
      max_date = curr_date

  # done
  return max_date
#end: most_frequent_date

# when somebody runs this file as a script: run main function
if __name__ == "__main__":
  main()

# (created using vim - the world's best text editor)
